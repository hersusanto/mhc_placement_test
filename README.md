## Documentation of MHC Placement Test

This application show list of all cryptocurrencies with latest market data from web www.coinmarketcap.com/api

How to use this application step by step :

## Login Page

To Log In username & password are needed,but in the meantime just use the default username & password given below :

- username : mhc_placement
- password : success

The Login Page preview can be found in the link below :
[a link](http://bit.ly/2DSicX3)

## Home Page

Some of the things you can find in the home page are :

1. Top 10 highest rank of cryptocurrencies
2. List all cryptocurrencies
3. Search specific keyword related to cryptocurrencies
4. Find the detail of specific cryptocurrencies

The Home Page preview can be found in the link below :
[a link](http://bit.ly/2DT6eMK)
[a link](http://bit.ly/2vIjOhK)

## Detail Page

User able to find the related keyword about specific cryptocurrencies and able to see the details in which included :

1. Current Price
2. Rank Position
3. Market Cap
4. Volume 24h
5. Circulating Supply
6. Find more detail from other sources like Website, Twitter, Reddit & Telegram

The Detail Page preview can be found in the link below :
[a link](http://bit.ly/2JlIzbn)
[a link](http://bit.ly/2J72Fae)

## News Feed Page

User able to find the latest news or headline from reliable sources and able to search by preferences.

The News Feed preview can be found in the link below :
[a link](http://bit.ly/2V52n55)
[a link](http://bit.ly/2V8F8XL)

## Settings Page

In the setting page user is only allowed to log out.

The Setting Page preview can be found in the link below :
[a link](http://bit.ly/3031ygM)

## Link APK

APK file can be downloaded here.
[a link](http://bit.ly/2VNr8Hj)
