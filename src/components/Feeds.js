import React, { Component } from "react";
import {
  Text,
  View,
  Button,
  Dimensions,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  Linking,
  TextInput
} from "react-native";
import axios from "axios";
import moment from "moment";
import Ionicons from "react-native-vector-icons/Ionicons";
import Loader from "../components/Loader";
import { TabView, SceneMap } from "react-native-tab-view";

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#ff4081" }]} />
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#673ab7" }]} />
);

export default class Feeds extends Component {
  constructor(props) {
    super(props);

    this.state = {
      feeds: [],
      value: ""
    };
    this.getUrl = this.getUrl.bind(this);
    this.handleClear = this.handleClear.bind(this);
  }
  componentDidMount() {
    let url = "https://min-api.cryptocompare.com/data/v2/news/?lang=EN",
      headers = {
        "x-api-key":
          "590a399aea5789c05b3e58766b26f2447b75da769dd3a5a9f3903fd94bb5b932"
      };
    axios.get(url, headers).then(res => {
      this.setState({
        feeds: res.data.Data
      });
    });
  }
  getUrl = url => {
    let str = url.split("");
    let idx = [];
    for (let i = 0; i < str.length; i++) {
      if (str[i] === "/") {
        idx.push(i);
      }
    }
    if (url[4] === "s") {
      return url.substring(8, idx[idx.length - 1]);
    } else {
      return url.substring(7, idx[idx.length - 1]);
    }
  };
  handleClear = () => {
    this.setState({
      value: ""
    });
  };

  render() {
    return (
      <View>
        <Text>Hello World</Text>
      </View>
    );
  }
}

export const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  scene: {
    flex: 1
  },
  container: {
    backgroundColor: "#12306A",
    width: width,
    marginBottom: 55,
    paddingBottom: 100
  },
  boxFeeds: {
    flex: 1,
    flexDirection: "row",
    marginHorizontal: 10,
    marginBottom: 15,
    paddingVertical: 10,
    paddingHorizontal: 15,
    width: width,
    left: -10,
    borderBottomWidth: 1,
    borderBottomColor: "white",
    alignItems: "center",
    backgroundColor: "#12306A"
  },
  bodyFeeds: {
    flex: 2,
    paddingHorizontal: 5,
    backgroundColor: "#12306A"
  },
  poster: {
    width: 0.25 * width,
    height: 90,
    borderRadius: 5,
    marginRight: 10
  },
  title: {
    color: "yellow",
    fontSize: 16,
    textAlign: "justify"
  },
  posterAuthor: {
    width: 30,
    height: 30
  },
  website: {
    fontSize: 12,
    color: "white",
    marginHorizontal: 10
  },
  searchBar: {
    marginTop: 15,
    marginHorizontal: 15,
    marginBottom: 15
  }
});
