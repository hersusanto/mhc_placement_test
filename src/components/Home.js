import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import axios from "axios";
import numeral from "numeral";
import Ionicons from "react-native-vector-icons/Ionicons";
import { REQUEST_LATEST_DATA } from "../redux/type/food";
import Loader from "../components/Loader";
// import Slideshow from "react-native-slideshow";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isSearch: false,
      value: "",
      detailInfo: [],
      addInfo: {}
    };
    this.handleClear = this.handleClear.bind(this);
    this.handleOrder = this.handleOrder.bind(this);
  }
  componentDidMount() {
    this.props.getCoinList();
  }

  handleClear() {
    this.setState({
      value: ""
    });
  }
  handleOrder(id) {
    console.log(id, "index");
    this.props.navigation.navigate("Detail", {
      item: this.props.coin[id]
    });
  }
  render() {
    console.log(this.props, "PROPS");
    let slides, foodList;
    if (this.props.count > 0) {
      slides = this.props.coin.slice(0, 5).map((coin, i) => {
        return (
          <TouchableOpacity onPress={() => this.handleOrder(i)}>
            <View style={{ marginHorizontal: 10 }}>
              <Image
                source={{
                  uri: coin.image_url
                }}
                style={{
                  width: width - 20,
                  height: 250,
                  marginVertical: 10,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 10
                }}
              />
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  justifyContent: "center",
                  alignItems: "flex-start",
                  marginLeft: 20
                }}
              >
                <Text
                  style={{
                    color: "#fff",
                    fontWeight: "bold",
                    textAlign: "center",
                    fontSize: 24
                  }}
                >
                  {coin.name}
                </Text>
                <Text
                  style={{
                    color: "#fff",
                    fontWeight: "bold",
                    textAlign: "center",
                    fontSize: 18
                  }}
                >
                  Rating : {coin.rating}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        );
      });
      foodList = this.props.coin
        .slice(5, this.props.coin.length)
        .map((coin, i) => {
          let categories = coin.categories.map(item => {
            return (
              <View
                style={{
                  flexWrap: "wrap",
                  backgroundColor: "#fff",
                  borderRadius: 5,
                  margin: 2,
                  borderWidth: 1,
                  borderColor: "#00AA13"
                  // padding: 5
                }}
              >
                <Text style={{ fontSize: 10, color: "#00AA13", padding: 3 }}>
                  {item.title}
                </Text>
              </View>
            );
          });
          return (
            <TouchableOpacity onPress={() => this.handleOrder(i)}>
              <View
                style={{
                  flex: 1,
                  marginHorizontal: 5,
                  marginTop: 10,
                  marginBottom: 5,
                  width: 180
                  // flexDirection: "column"
                }}
              >
                <Image
                  source={{
                    uri: coin.image_url
                  }}
                  style={{
                    width: 180,
                    height: 180,
                    // marginVertical: 10,
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 10
                    // left: 25
                  }}
                />
                <View
                  style={{
                    flexDirection: "column",
                    height: 100,
                    width: 180,
                    marginTop: 15,
                    marginBottom: -60
                  }}
                >
                  <Text
                    style={{
                      color: "black",
                      fontWeight: "bold",
                      textAlign: "left",
                      fontSize: 15,
                      flex: 1,
                      flexShrink: 1,
                      flexWrap: "wrap"
                    }}
                  >
                    {coin.name}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    // marginTop: -30,
                    flexWrap: "wrap",
                    paddingVertical: 3,
                    justifyContent: "flex-start"
                  }}
                >
                  {categories}
                </View>
              </View>
            </TouchableOpacity>
          );
        });
    }
    return this.props.count > 0 ? (
      <View>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          style={{ marginBottom: 20 }}
        >
          {slides}
        </ScrollView>
        <TouchableOpacity
        // onPress={() => this.handleLogin()}
        >
          <View style={{ justifyContent: "center", marginHorizontal: 20 }}>
            <Text
              style={{
                alignItems: "center",
                backgroundColor: "#00AA13",
                borderRadius: 15,
                color: "#fff",
                textAlign: "center",
                paddingVertical: 10,
                fontSize: 20,
                fontWeight: "bold",
                marginBottom: 20
              }}
            >
              Donate
            </Text>
          </View>
        </TouchableOpacity>
        <ScrollView horizontal={false} showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexWrap: "wrap",
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            {foodList}
          </View>
        </ScrollView>
      </View>
    ) : (
      <Loader />
    );
  }

  //   render() {
  //     console.log(this.props, "PROPS");
  //     let data;
  //     this.props.count > 0
  //       ? (data = this.props.coin)
  //       : (data = [
  //           {
  //             id: 1376,
  //             name: "NEO",
  //             symbol: "NEO",
  //             slug: "neo",
  //             circulating_supply: 65000000,
  //             total_supply: 100000000,
  //             max_supply: 100000000,
  //             date_added: "2016-09-08T00:00:00.000Z",
  //             num_market_pairs: 173,
  //             tags: [],
  //             platform: null,
  //             cmc_rank: 19,
  //             last_updated: "2019-05-05T06:26:02.000Z",
  //             quote: {
  //               USD: {
  //                 price: 9.56829120648,
  //                 volume_24h: 302480769.800639,
  //                 percent_change_1h: 0.519252,
  //                 percent_change_24h: -2.74749,
  //                 percent_change_7d: 0.374891,
  //                 market_cap: 621938928.4211999,
  //                 last_updated: "2019-05-05T06:26:02.000Z"
  //               }
  //             }
  //           },
  //           {
  //             id: 2566,
  //             name: "Ontology",
  //             symbol: "ONT",
  //             slug: "ontology",
  //             circulating_supply: 494804358,
  //             total_supply: 1000000000,
  //             max_supply: null,
  //             date_added: "2018-03-08T00:00:00.000Z",
  //             num_market_pairs: 70,
  //             tags: [],
  //             platform: null,
  //             cmc_rank: 20,
  //             last_updated: "2019-05-05T06:26:06.000Z",
  //             quote: {
  //               USD: {
  //                 price: 1.09895207242,
  //                 volume_24h: 67521815.5011175,
  //                 percent_change_1h: 1.36084,
  //                 percent_change_24h: -4.00879,
  //                 percent_change_7d: -4.9367,
  //                 market_cap: 543766274.6665475,
  //                 last_updated: "2019-05-05T06:26:06.000Z"
  //               }
  //             }
  //           },
  //           {
  //             id: 1376,
  //             name: "NEO",
  //             symbol: "NEO",
  //             slug: "neo",
  //             circulating_supply: 65000000,
  //             total_supply: 100000000,
  //             max_supply: 100000000,
  //             date_added: "2016-09-08T00:00:00.000Z",
  //             num_market_pairs: 173,
  //             tags: [],
  //             platform: null,
  //             cmc_rank: 19,
  //             last_updated: "2019-05-05T06:26:02.000Z",
  //             quote: {
  //               USD: {
  //                 price: 9.56829120648,
  //                 volume_24h: 302480769.800639,
  //                 percent_change_1h: 0.519252,
  //                 percent_change_24h: -2.74749,
  //                 percent_change_7d: 0.374891,
  //                 market_cap: 621938928.4211999,
  //                 last_updated: "2019-05-05T06:26:02.000Z"
  //               }
  //             }
  //           },
  //           {
  //             id: 2566,
  //             name: "Ontology",
  //             symbol: "ONT",
  //             slug: "ontology",
  //             circulating_supply: 494804358,
  //             total_supply: 1000000000,
  //             max_supply: null,
  //             date_added: "2018-03-08T00:00:00.000Z",
  //             num_market_pairs: 70,
  //             tags: [],
  //             platform: null,
  //             cmc_rank: 20,
  //             last_updated: "2019-05-05T06:26:06.000Z",
  //             quote: {
  //               USD: {
  //                 price: 1.09895207242,
  //                 volume_24h: 67521815.5011175,
  //                 percent_change_1h: 1.36084,
  //                 percent_change_24h: -4.00879,
  //                 percent_change_7d: -4.9367,
  //                 market_cap: 543766274.6665475,
  //                 last_updated: "2019-05-05T06:26:06.000Z"
  //               }
  //             }
  //           }
  //         ]);

  //     let totalVolume,
  //       totalMarketCap = 0,
  //       btcDominance,
  //       topTen,
  //       coins;
  //     if (data) {
  //       totalVolume = data.map((coin, i) => {
  //         return coin.quote.USD.volume_24h++;
  //       });
  //       data.forEach(value => {
  //         totalMarketCap += value.quote.USD.market_cap;
  //       });
  //       btcDominance = (data[0].quote.USD.market_cap / totalMarketCap) * 100;

  //       topTen = data.slice(0, 10).map((coin, i) => {
  //         return (
  //           <TouchableOpacity onPress={() => this.handleDetail(coin.id)}>
  //             <View
  //               style={{
  //                 padding: 10,
  //                 backgroundColor: "#bf9b30",
  //                 alignItems: "center",
  //                 margin: 5,
  //                 borderRadius: 10,
  //                 width: 90,
  //                 height: 120,
  //                 elevation: 15,
  //                 marginVertical: 10
  //               }}
  //             >
  //               <Text
  //                 style={{
  //                   color: "white",
  //                   textAlign: "center",
  //                   fontSize: 8,
  //                   position: "absolute"
  //                 }}
  //               >
  //                 {coin.cmc_rank}
  //               </Text>
  //               <Text
  //                 style={{
  //                   color: "white",
  //                   fontWeight: "bold",
  //                   textAlign: "center"
  //                 }}
  //               >
  //                 {coin.name.slice(0, 8)}
  //               </Text>
  //               <Image
  //                 source={{
  //                   uri: `https://s2.coinmarketcap.com/static/img/coins/64x64/${coin.id}.png`
  //                 }}
  //                 style={{
  //                   width: 40,
  //                   height: 40,
  //                   marginVertical: 10,
  //                   alignItems: "center",
  //                   justifyContent: "center"
  //                   // left: 25
  //                 }}
  //               />
  //               <Text
  //                 style={{
  //                   color: "white",
  //                   fontWeight: "bold",
  //                   textAlign: "center"
  //                 }}
  //               >
  //                 {numeral(coin.quote.USD.price).format("$ 0,0.00")}
  //               </Text>
  //             </View>
  //           </TouchableOpacity>
  //         );
  //       });
  //       if (this.state.value) {
  //         data = this.props.coin
  //           .map(datum => {
  //             if (
  //               datum.name
  //                 .toLowerCase()
  //                 .includes(this.state.value.toLocaleLowerCase())
  //             ) {
  //               return datum;
  //             }
  //           })
  //           .filter(data => data !== undefined);
  //       } else {
  //         data = this.props.coin;
  //       }
  //       coins = data.map((coin, i) => {
  //         return (
  //           <TouchableOpacity onPress={() => this.handleDetail(coin.id)}>
  //             <View style={styles.box}>
  //               <View style={styles.coin}>
  //                 <View style={{ flex: 1 }}>
  //                   <Text
  //                     style={{
  //                       fontSize: 10,
  //                       position: "absolute",
  //                       fontWeight: "bold",
  //                       color: "black",
  //                       left: -5,
  //                       top: -5
  //                     }}
  //                   >
  //                     {coin.cmc_rank}
  //                   </Text>
  //                   <Image
  //                     source={{
  //                       uri: `https://s2.coinmarketcap.com/static/img/coins/64x64/${coin.id}.png`
  //                     }}
  //                     style={styles.logo}
  //                   />
  //                 </View>
  //                 <View style={{ flex: 1 }}>
  //                   <Text style={styles.symbol}>{coin.symbol}</Text>
  //                   <Text style={styles.name}>{coin.name}</Text>
  //                 </View>
  //               </View>
  //               <View style={styles.price}>
  //                 <Text style={styles.priceUp}>
  //                   {numeral(coin.quote.USD.price).format("$ 0,0.00")}
  //                 </Text>
  //                 <Text
  //                   style={[
  //                     styles.priceBottom,
  //                     {
  //                       color:
  //                         coin.quote.USD.percent_change_24h < 0 ? "red" : "green"
  //                     }
  //                   ]}
  //                 >
  //                   {coin.quote.USD.percent_change_24h.toFixed(2)} %
  //                 </Text>
  //               </View>
  //               <View style={styles.capVol}>
  //                 <Text style={styles.volUp}>
  //                   {numeral(coin.quote.USD.market_cap)
  //                     .format("($ 0.00 a)")
  //                     .toUpperCase()}
  //                 </Text>
  //                 <Text style={styles.volBottom}>
  //                   {numeral(coin.quote.USD.volume_24h)
  //                     .format("($ 0.00 a)")
  //                     .toUpperCase()}
  //                 </Text>
  //               </View>
  //             </View>
  //           </TouchableOpacity>
  //         );
  //       });
  //     }
  //     return this.props.count > 0 ? (
  //       <View style={styles.container}>
  //         <View
  //           style={{
  //             backgroundColor: "#092757",
  //             width: width,
  //             left: -10,
  //             paddingHorizontal: 20,
  //             paddingVertical: 10,
  //             elevation: 15
  //           }}
  //         >
  //           <View
  //             style={[
  //               styles.boxTotal,
  //               {
  //                 width: width,
  //                 left: -20,
  //                 marginTop: -20,
  //                 marginBottom: -15,
  //                 backgroundColor: "#092757"
  //               }
  //             ]}
  //           >
  //             <ScrollView
  //               showsHorizontalScrollIndicator={false}
  //               horizontal={true}
  //             >
  //               {topTen}
  //             </ScrollView>
  //           </View>
  //           <View style={styles.searchBar}>
  //             <TextInput
  //               style={{
  //                 backgroundColor: "#283571",
  //                 borderRadius: 10,
  //                 paddingHorizontal: 40,
  //                 paddingVertical: 5,
  //                 color: "white"
  //               }}
  //               placeholder="Search coins..."
  //               placeholderTextColor="white"
  //               onChangeText={value => this.setState({ value })}
  //               value={this.state.value}
  //             />
  //             <Ionicons
  //               name="ios-search"
  //               size={25}
  //               color="white"
  //               style={{ position: "absolute", top: 5, left: 10 }}
  //             />
  //             <Ionicons
  //               name="ios-close-circle-outline"
  //               size={25}
  //               color={this.state.value ? "white" : "#283571"}
  //               style={{ position: "absolute", top: 5, right: 10 }}
  //               onPress={() => this.handleClear()}
  //             />
  //           </View>
  //         </View>
  //         <View style={{ marginBottom: 210 }}>
  //           <View style={styles.boxTotal}>
  //             <View style={styles.boxDominance}>
  //               <Text style={styles.lableTotal}>BTC DOMINANCE</Text>
  //               <Text style={styles.valueTotal}>{btcDominance.toFixed(2)} %</Text>
  //             </View>
  //             <View style={styles.boxMarkectCap}>
  //               <Text style={styles.lableTotal}>TOTAL MARKET CAP</Text>
  //               <Text style={styles.valueTotal}>
  //                 {numeral(totalMarketCap)
  //                   .format("($ 0.00 a)")
  //                   .toUpperCase()}
  //               </Text>
  //             </View>
  //             <View style={styles.boxVolume}>
  //               <Text style={styles.lableTotal}>TOTAL VOLUME</Text>
  //               <Text style={styles.valueTotal}>
  //                 {numeral(totalVolume[0])
  //                   .format("($ 0.00 a)")
  //                   .toUpperCase()}
  //               </Text>
  //             </View>
  //           </View>
  //           <View style={styles.boxHeader}>
  //             <View style={styles.boxHeaderCoin}>
  //               <Text style={styles.headerCoin}>COIN</Text>
  //             </View>
  //             <View style={styles.boxHeaderPrice}>
  //               <Text style={styles.headerPrice}>PRICE / 24H CHG</Text>
  //             </View>
  //             <View style={styles.boxHeaderPrice}>
  //               <Text style={styles.headerPrice}>M.CAP / VOL</Text>
  //             </View>
  //           </View>
  //           <ScrollView
  //             showsVerticalScrollIndicator={false}
  //             style={{ marginBottom: 75, paddingBottom: 100 }}
  //           >
  //             {coins}
  //           </ScrollView>
  //         </View>
  //       </View>
  //     ) : (
  //       <Loader />
  //     );
  //   }
}

const mapStateToProps = state => ({
  // foods: state.food.data
  coin: state.coin.data,
  count: state.coin.count
});
const mapDispatchToProps = dispatch => {
  return {
    getCoinList: () => dispatch({ type: REQUEST_LATEST_DATA })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

export const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#283571",
    paddingHorizontal: 10
  },
  boxTotal: {
    flexDirection: "row",
    marginVertical: 0,
    paddingVertical: 10
  },
  boxDominance: {
    flex: 1,
    justifyContent: "flex-start"
  },
  boxMarkectCap: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  boxVolume: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  lableTotal: {
    fontSize: 10,
    color: "yellow",
    fontWeight: "bold"
  },
  valueTotal: {
    fontSize: 12,
    color: "white"
  },
  searchBar: {
    marginTop: 15
  },
  box: {
    flexDirection: "row",
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: "#EFF4FC",
    marginTop: 10,
    borderRadius: 5,
    elevation: 15
  },
  coin: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    color: "white"
  },
  price: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  capVol: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  logo: {
    height: 40,
    width: 40
  },
  symbol: {
    fontSize: 14,
    color: "#7b7d7b",
    fontWeight: "bold"
  },
  name: {
    color: "grey",
    fontSize: 12
  },
  priceUp: {
    fontSize: 14,
    color: "#7b7d7b",
    textAlign: "right"
  },
  priceBottom: {
    color: "grey",
    textAlign: "right",
    fontSize: 12
  },
  volUp: {
    fontSize: 14,
    color: "#7b7d7b",
    textAlign: "right"
  },
  volBottom: {
    color: "grey",
    textAlign: "right",
    fontSize: 12
  },
  boxHeader: {
    flex: 1,
    flexDirection: "row",
    paddingVertical: 10,
    paddingHorizontal: 15,
    // backgroundColor: "#44475a",
    marginTop: 10,
    marginBottom: 0
  },
  headerCoin: {
    fontSize: 10,
    color: "yellow"
  },
  headerPrice: {
    fontSize: 10,
    color: "yellow"
  },
  boxHeaderCoin: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
    left: 15
  },
  boxHeaderPrice: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end"
  }
});
