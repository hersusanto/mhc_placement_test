import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  ListView
} from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import { Sae } from "react-native-textinput-effects";
import logo from "../assets/nopay.jpg";
import SplashScreen from "react-native-splash-screen";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: ""
    };
    this.handleLogin = this.handleLogin.bind(this);
  }
  componentDidMount() {
    SplashScreen.hide();
  }
  handleLogin() {
    // this.props.navigation.navigate("Home");
    if (this.state.username === "user01" && this.state.password === "123") {
      this.props.navigation.navigate("Home");
    } else {
      alert(`Use username "user01" & password "123"`);
      this.setState({
        username: "",
        password: ""
      });
    }
  }
  render() {
    return (
      <ScrollView
        style={styles.boxContainer}
        showsVerticalScrollIndicator={false}
      >
        <ImageBackground style={styles.logo} source={logo} />
        <View style={styles.container}>
          <Sae
            inputStyle={styles.input}
            labelStyle={styles.label}
            label={"Username"}
            iconClass={FontAwesomeIcon}
            iconColor={"white"}
            iconName={"pencil-square-o"}
            inputPadding={16}
            labelHeight={24}
            borderHeight={2}
            borderColor={"blue"}
            autoCapitalize={"none"}
            autoCorrect={false}
            onChangeText={username => this.setState({ username })}
            value={this.state.username}
          />
          <Sae
            inputStyle={styles.input}
            labelStyle={styles.label}
            label={"Password"}
            iconClass={FontAwesomeIcon}
            iconColor={"white"}
            iconName={"pencil-square-o"}
            inputPadding={16}
            labelHeight={24}
            borderHeight={2}
            autoCapitalize={"none"}
            autoCorrect={false}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
            secureTextEntry={true}
          />

          <TouchableOpacity onPress={() => this.handleLogin()}>
            <Text style={styles.button}>Log In</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

export const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  boxContainer: {
    backgroundColor: "#fff"
  },
  container: {
    paddingHorizontal: 45,
    top: 0.5 * height,
    position: "absolute",
    width: width
  },
  logo: {
    alignItems: "center",
    resizeMode: "contain",
    marginBottom: -35,
    width: width,
    height: height
  },
  label: {
    color: "white",
    fontWeight: "normal",
    fontSize: 8,
    paddingBottom: 5
  },
  input: {
    color: "white",
    borderBottomColor: "white",
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingBottom: 5,
    height: 50,
    fontSize: 14
  },
  button: {
    marginTop: 40,
    height: 40,
    alignItems: "center",
    backgroundColor: "#00AA13",
    borderRadius: 60,
    color: "#fff",
    textAlign: "center",
    padding: 6,
    fontSize: 16,
    fontWeight: "bold",
    width: 0.4 * width,
    left: 0.18 * width
  }
});
